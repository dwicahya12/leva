package com.mobile.tubes.multiuser;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.mobile.tubes.multiuser.model.Attraction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActivityBayar extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    TextView date,txDate, txharga1;
    private int mYear;
    private int mMonth;
    private int mDay;
    static final int DATE_DIALOG_ID = 0;
    Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar);
        setTitle("Bayar Wisata");
        final Calendar myCalendar = Calendar.getInstance();

        date = findViewById(R.id.date);
        txDate = findViewById(R.id.txDate);
        txharga1 = findViewById(R.id.txharga1);

//        Attraction attraction = getIntent().getParcelableExtra("attraction");
//
//        txharga1.setText("Rp "+attraction.getHarga());

        intent = new Intent(ActivityBayar.this, Bayar2Activity.class);



        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        // get the current date


        // display the current date
        updateDisplay();

    }

    private void updateDisplay() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        this.txDate.setText(DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime()));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        txDate.setText(currentDateString);

        intent.putExtra("date", currentDateString);
    }

    public void gotoBayar2(View view){
        startActivity(intent);
        finish();
    }
}
