package com.mobile.tubes.multiuser;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Admin extends AppCompatActivity {
Button btnpush_1, btnpush_2, btnpush_3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        btnpush_1 = (Button)findViewById(R.id.btn_1);
        btnpush_2 = (Button)findViewById(R.id.btn_2);
        btnpush_3 = (Button)findViewById(R.id.btn_3);


        btnpush_1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin.this,WaktuPush.class);
                startActivity(i);
            }
        });

        btnpush_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin.this,WaktuPush.class);
                startActivity(i);
            }
        });

        btnpush_3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin.this,WaktuPush.class);
                startActivity(i);
            }
        });
    }

    public void push(View view){
        startActivity(new Intent(Admin.this,WaktuPush.class));
        finish();
    }


}
