package com.mobile.tubes.multiuser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.mobile.tubes.multiuser.model.Attraction;
import com.mobile.tubes.multiuser.model.AttractionCollection;
import com.mobile.tubes.multiuser.model.AttractionRepository;

import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
public class AttractionPagerActivity extends AppCompatActivity {
    /* Class variables */
    private ViewPager viewPager;
    private List<Attraction> attractions;
    private String sectionTitle;

    /* Class Constants */
    private static final String EXTRA_SECTION_TITLE = "com.mobile.tubes.multiuser.section_title";
    private static final String EXTRA_ATTRACTION_TITLE = "com.mobile.tubes.multiuser.attraction_title";

    public static Intent newIntent(Context packageContext, String sectionTitle, String attractionTitle) {
        Intent intent = new Intent(packageContext, AttractionPagerActivity.class);
        intent.putExtra(EXTRA_SECTION_TITLE, sectionTitle);
        intent.putExtra(EXTRA_ATTRACTION_TITLE, attractionTitle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction_pager);

        sectionTitle = getIntent().getStringExtra(EXTRA_SECTION_TITLE);
        if (Integer.parseInt(sectionTitle) == 0) {
            throw new IllegalArgumentException("AttractionListAdapter has not reported on section title");
        } else {
            List<AttractionCollection> collections = AttractionRepository.getInstance(this).getCollections();
            for (AttractionCollection collection : collections) {
                if (collection.getHeaderTitle() == sectionTitle ) {
                    attractions = collection.getAttractions();
                    break;
                }
            }
        }

        String attractionTitle = getIntent().getStringExtra(EXTRA_ATTRACTION_TITLE);
        if (Integer.parseInt(attractionTitle) == 0) {
            throw new IllegalArgumentException("AttractionListAdapter has not reported on attraction title");
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        viewPager = findViewById(R.id.attraction_view_pager);
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public int getCount() {
                return attractions.size();
            }

            @Override
            public Fragment getItem(int position) {
                Attraction attraction = attractions.get(position);
                return AttractionFragment.newInstance(attraction);
            }
        });

        for (int i = 0; i < attractions.size(); i++) {
            if (attractionTitle == attractions.get(i).getTitle()) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
