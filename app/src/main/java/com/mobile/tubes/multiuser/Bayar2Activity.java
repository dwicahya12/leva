package com.mobile.tubes.multiuser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.tubes.multiuser.model.Attraction;

public class Bayar2Activity extends AppCompatActivity {

    Spinner sp1,sp2,sp3,sp4,sp5;
    TextView txDate2, txharga2, txtitlebayar;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar2);

        sp1 = findViewById(R.id.spPay1);
        sp2 = findViewById(R.id.spPay2);
        sp3 = findViewById(R.id.spPay3);
        sp4 = findViewById(R.id.spPay4);
        sp5 = findViewById(R.id.spPay5);
        txDate2 = findViewById(R.id.txDate2);
        txharga2 = findViewById(R.id.txharga2);
        txtitlebayar = findViewById(R.id.txtitlebayar2);
        btn = findViewById(R.id.confirmpay);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.paymethod, R.layout.support_simple_spinner_dropdown_item);
        sp1.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.paymethod2, R.layout.support_simple_spinner_dropdown_item);
        sp2.setAdapter(adapter2);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.paymethod3, R.layout.support_simple_spinner_dropdown_item);
        sp3.setAdapter(adapter3);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this, R.array.paymethod4, R.layout.support_simple_spinner_dropdown_item);
        sp4.setAdapter(adapter4);
        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(this, R.array.paymethod5, R.layout.support_simple_spinner_dropdown_item);
        sp5.setAdapter(adapter5);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });

    }

    public void openDialog(){
        AlertPaymentDialog alertPaymentDialog = new AlertPaymentDialog();
        alertPaymentDialog.show(getSupportFragmentManager(),"alert dialog");
    }



}
