package com.mobile.tubes.multiuser;

public class Card {
    String imageResourceId,titleTextResId,shortDescTextResId,longDescTextResId,mapQueryStrId;

    public Card(String imageResourceId, String titleTextResId, String shortDescTextResId,
                String longDescTextResId, String mapQueryStrId) {
        this.imageResourceId = imageResourceId;
        this.titleTextResId = titleTextResId;
        this.shortDescTextResId = shortDescTextResId;
        this.longDescTextResId = longDescTextResId;
        this.mapQueryStrId = mapQueryStrId;
    }

    public String getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(String imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public String gettitleTextResId() {
        return titleTextResId;
    }

    public void settitleTextResId(String titleTextResId) {
        this.titleTextResId = titleTextResId;
    }

    public String getshortDescTextResId() {
        return shortDescTextResId;
    }

    public void setshortDescTextResId(String shortDescTextResId) {
        this.shortDescTextResId = shortDescTextResId;
    }

    public String getlongDescTextResId() {
        return longDescTextResId;
    }

    public void setlongDescTextResId(String longDescTextResId) {
        this.longDescTextResId = longDescTextResId;
    }

    public String getmapQueryStrId() {
        return mapQueryStrId;
    }

    public void setmapQueryStrId(String mapQueryStrId) {
        this.mapQueryStrId = mapQueryStrId;
    }

}
