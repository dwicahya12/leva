package com.mobile.tubes.multiuser;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class DashboardPengelola extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Card> daftar;
    AdapterCard adapterCard;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    ProgressBar progressBarMain;
    int REQUEST_MENU = 404;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_pengelola);
        mAuth = FirebaseAuth.getInstance();

        recyclerView = findViewById(R.id.RVPengelola);
        daftar = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(DashboardPengelola.this));
        adapterCard = new AdapterCard(daftar, DashboardPengelola.this);
        recyclerView.setAdapter(adapterCard);
        db = FirebaseFirestore.getInstance();

        init();
    }

    @SuppressLint("StaticFieldLeak")
    public void init() {
        daftar.clear();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }

            @Override
            protected Void doInBackground(Void... voids) {

                getthat();
                return null;
            }
        }.execute();

    }

    public void loadMenu(View view) {
        init();
    }

    public void getthat() {
        db.collection(mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        daftar.add(new Card(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString()));
                    }
                    adapterCard.notifyDataSetChanged();

                }
                progressBarMain.setVisibility(View.GONE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        init();
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void gotoTambahWisata(View view){
        startActivity(new Intent(DashboardPengelola.this,TambahWisata.class));
        finish();
    }

    public void gotoReqPush(View view){
        startActivity(new Intent(DashboardPengelola.this,ActivityRequestPush.class));
        finish();
    }

}
