package com.mobile.tubes.multiuser;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.tubes.multiuser.model.Attraction;

import java.io.ByteArrayInputStream;

public class DetailTempat extends AppCompatActivity {

    TextView title, harga, deskripsi;
    ImageView gambar;
    Button btnMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tempat);

        title = findViewById(R.id.TitleWisata);
        harga = findViewById(R.id.txHarga);
        deskripsi = findViewById(R.id.detail_long_desc_tv2);
        gambar = findViewById(R.id.detail_image_view2);
        btnMaps = findViewById(R.id.detail_show_in_map_button2);

        Attraction attraction = getIntent().getParcelableExtra("attraction");

        title.setText(attraction.getTitle());
        harga.setText("Rp "+attraction.getHarga());
        deskripsi.setText(attraction.getLongDesc());

        Intent intentabc = new Intent(DetailTempat.this, ActivityBayar.class);
        String abc = attraction.getTitle();
        String def = attraction.getHarga();
        intentabc.putExtra("title", abc);
        intentabc.putExtra("harga", def);


        final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + attraction.getImageResourceId());

        final long ONE_MEGABYTE = 10* 1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Drawable d = Drawable.createFromStream(new ByteArrayInputStream(bytes), null);
                gambar.setImageDrawable(d);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                gambar.setImageResource(R.drawable.ic_launcher_background);
            }
        });

        btnMaps.setOnClickListener(v -> {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("geo:0,0?q="+attraction.getMapQueryStrId()));
            startActivity(intent);
        });

        Toast.makeText(getApplicationContext(), attraction.getTitle(), Toast.LENGTH_SHORT).show();

    }


    public void goBayarActivity2 (View view){
        startActivity(new Intent(DetailTempat.this,ActivityBayar.class));
        finish();
    }
}
