package com.mobile.tubes.multiuser;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mobile.tubes.multiuser.model.Attraction;
import com.mobile.tubes.multiuser.model.AttractionCollection;
import com.mobile.tubes.multiuser.model.AttractionRepository;

import java.util.ArrayList;
import java.util.List;

public class Index extends AppCompatActivity  {

    ViewFlipper flipper;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    DrawerLayout mDrawerlayout;
    ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerlayout, R.string.open, R.string.close);
        mDrawerlayout.addDrawerListener(mToggle);
        mToggle.syncState();

        int images[] =  {R.drawable.promo2, R.drawable.promo1, R.drawable.promo3};

        flipper = findViewById(R.id.flipper);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        //looping
        for (int image:images){
            flipperImages(image);
        }
        //

        // Initialize list to store collection of attractions
        AttractionRepository repository = AttractionRepository.getInstance(this);
        List<AttractionCollection> collections = repository.getCollections();

        // Hook the recycler view
        RecyclerView recyclerView = findViewById(R.id.main_recycler_view);

        // Set fixed size true and optimize recycler view performance
        // The data container has fixed number of attractions and not infinite list
        recyclerView.setHasFixedSize(true);

        // Connect the RecyclerView widget to the vertical linear layout
        // (not reverse layout - hence false)
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        // Attach adapter to the RecyclerView widget which is connected to a layout manager
        MasterAdapter collectionAdapter = new MasterAdapter(this, collections);
        recyclerView.setAdapter(collectionAdapter);
    }

    public void flipperImages(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        flipper.addView(imageView);
        flipper.setFlipInterval(3000);
        flipper.setAutoStart(true);

        //animation
        flipper.setInAnimation(this, android.R.anim.slide_in_left);
        flipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

//    public void klik (View view){
//        Intent intent = new Intent(Index.this, recyclerview.class);
//        startActivity(intent);
//    }

}
