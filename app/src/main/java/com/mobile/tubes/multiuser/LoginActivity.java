package com.mobile.tubes.multiuser;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements TextWatcher,
        CompoundButton.OnCheckedChangeListener {
    EditText email, pass;
    FirebaseAuth mAuth;
    Spinner _spinner;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CheckBox cbForgot;
    private static final String TAG = "LoginActivity";
    private static final String PREF_NAME = "prefs";
    private static final String KEY_REMEMBER = "remember";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASS = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(LoginActivity.this);
        setContentView(R.layout.activity_login);
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        email = findViewById(R.id.edmailLogin);
        pass = findViewById(R.id.edpassLogin);
        mAuth = FirebaseAuth.getInstance();
        _spinner = (Spinner)findViewById(R.id.spinner);
        cbForgot = findViewById(R.id.cbForgot);

        if(sharedPreferences.getBoolean(KEY_REMEMBER, false))
            cbForgot.setChecked(true);
        else
            cbForgot.setChecked(false);

        email.setText(sharedPreferences.getString(KEY_USERNAME,""));
        pass.setText(sharedPreferences.getString(KEY_PASS,""));

        email.addTextChangedListener(this);
        pass.addTextChangedListener(this);
        cbForgot.setOnCheckedChangeListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.usertype, R.layout.support_simple_spinner_dropdown_item);
        _spinner.setAdapter(adapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        managePrefs();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        managePrefs();
    }

    private void managePrefs(){
        if(cbForgot.isChecked()){
            editor.putString(KEY_USERNAME, email.getText().toString().trim());
            editor.putString(KEY_PASS, pass.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER, true);
            editor.apply();
        }else{
            editor.putBoolean(KEY_REMEMBER, false);
            editor.remove(KEY_PASS);//editor.putString(KEY_PASS,"");
            editor.remove(KEY_USERNAME);//editor.putString(KEY_USERNAME, "");
            editor.apply();
        }
    }

    public boolean check(){
        if (email.getText().toString().equals("")){
            email.setError("Isi Emailmu");
            email.requestFocus();
            return false;
        }
        if (pass.getText().toString().equals("")){
            pass.setError("Isi Passwordmu");
            pass.requestFocus();
            return false;
        }
        return true;
    }

    public void ForgotPassword (View view){
        if (email.getText().toString().equals("")){
            email.setError("Isi Emailmu");
            email.requestFocus();
        } else  {
            mAuth.sendPasswordResetEmail(email.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(LoginActivity.this, "Email sent.", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Email sent.");
                            }
                        }
                    });
        }

    }

    public void login(View view){
        if (check()) {
            new AsyncTask<Void,Void,Boolean>(){
                @Override
                protected Boolean doInBackground(Void... voids) {
                    mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        String item = _spinner.getSelectedItem().toString();

                                        if (item.equals("Customer")){
                                            Intent intent = new Intent(LoginActivity.this, Index.class);
                                            startActivity(intent);
                                        } else  if (item.equals("Pengelola Wisata")){
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(intent);
                                        } else  if (item.equals("Admin")) {
                                            Intent intent = new Intent(LoginActivity.this, Admin.class);
                                            startActivity(intent);
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
                                        }
//                                        startActivity(new Intent(LoginActivity.this, Index.class));
                                        finish();
                                    } else {
                                        Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    return null;
                }

                @Override
                protected void onPreExecute() {
                    Toast.makeText(LoginActivity.this, "Sign In...", Toast.LENGTH_SHORT).show();

                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                }
            }.execute();

        }
    }

    public void GoToRegist(View view){
        startActivity(new Intent(LoginActivity.this,Regist.class));
        finish();
    }

}