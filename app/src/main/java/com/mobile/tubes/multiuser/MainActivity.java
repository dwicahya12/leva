package com.mobile.tubes.multiuser;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mobile.tubes.multiuser.model.Attraction;
import com.mobile.tubes.multiuser.model.AttractionCollection;
import com.mobile.tubes.multiuser.model.AttractionRepository;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView recyclerView;
    ArrayList<Card> daftar;
    AdapterCard adapterCard;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    ProgressBar progressBarMain;
    int REQUEST_MENU = 404;
//    ViewFlipper flipper;
    DrawerLayout mDrawerlayout;
    ActionBarDrawerToggle mToggle;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerlayout, R.string.open, R.string.close);
        mDrawerlayout.addDrawerListener(mToggle);
        mToggle.syncState();
        NavigationView navigationView = (NavigationView)findViewById(R.id.nv1);
        navigationView.setNavigationItemSelectedListener(this);

        actionBar = getSupportActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("");
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        progressBarMain = findViewById(R.id.progressBar3);

        recyclerView = findViewById(R.id.main_recycler_view2);
        daftar = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        adapterCard = new AdapterCard(daftar, MainActivity.this);
        recyclerView.setAdapter(adapterCard);


        init();

        int images[] =  {R.drawable.promo2, R.drawable.promo1, R.drawable.promo3};

//        flipper = findViewById(R.id.flipper);

        //looping
        for (int image:images){
            flipperImages(image);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        //

//        // Initialize list to store collection of attractions
//        AttractionRepository repository = AttractionRepository.getInstance(this);
//        List<AttractionCollection> collections = repository.getCollections();
//
//        // Hook the recycler view
//        RecyclerView recyclerView = findViewById(R.id.main_recycler_view);
//
//        // Set fixed size true and optimize recycler view performance
//        // The data container has fixed number of attractions and not infinite list
//        recyclerView.setHasFixedSize(true);
//
//        // Connect the RecyclerView widget to the vertical linear layout
//        // (not reverse layout - hence false)
//        recyclerView.setLayoutManager(new LinearLayoutManager(this,
//                LinearLayoutManager.VERTICAL, false));
//
//        // Attach adapter to the RecyclerView widget which is connected to a layout manager
//        MasterAdapter collectionAdapter = new MasterAdapter(this, collections);
//        recyclerView.setAdapter(collectionAdapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void flipperImages(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

//        flipper.addView(imageView);
//        flipper.setFlipInterval(3000);
//        flipper.setAutoStart(true);
//
//        //animation
//        flipper.setInAnimation(this, android.R.anim.slide_in_left);
//        flipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

//    public void klik (View view){
//        Intent intent = new Intent(Index.this, recyclerview.class);
//        startActivity(intent);
//    }


    // ---------------------------------------------------------------------

    @SuppressLint("StaticFieldLeak")
    public void init() {
        daftar.clear();
        progressBarMain.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }

            @Override
            protected Void doInBackground(Void... voids) {

                getthat();
                return null;
            }
        }.execute();

    }

    public void loadMenu(View view) {
        init();
    }

    public void getthat() {
        FirebaseFirestore.getInstance().collection("wisata")
                .whereEqualTo("UID",mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());

                        daftar.add(new Card(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString()));
                    }
                    //String imageResourceId, String titleTextResId, String shortDescTextResId,
                    //                String longDescTextResId, String mapQueryStrId)
                    adapterCard.notifyDataSetChanged();

                }
                progressBarMain.setVisibility(View.GONE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        init();
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void GoToRegist(View view){
        startActivity(new Intent(MainActivity.this,Regist.class));
        finish();
    }

    public void gotoTambahWisata(View view){
        startActivity(new Intent(MainActivity.this,TambahWisata.class));
        finish();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.logout){
            logout();
        }

        return false;
    }

    private void logout() {
        mAuth.signOut();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }
}
