package com.mobile.tubes.multiuser.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Attraction implements Parcelable {

    /** Image resource ID for the image file associated with the attraction */
    private String imageResourceId;

    /** Title of the attraction */
    private String titleTextResId;

    /** Brief description of the attraction */
    private String shortDescTextResId;

    /** Long description of the attraction */
    private String longDescTextResId;

    /** Map Query String of attraction */
    private String mapQueryStrId;

    private String harga;

    /**
     * Create data object that holds all the details of an attraction including an image resource
     * for the attraction
     *
     * @param imageResourceId    an integer value for the image resource ID
     * @param titleTextResId     a String value for the name of the attraction
     * @param shortDescTextResId a String value for a brief description of the attraction
     */
    public Attraction(String imageResourceId, String titleTextResId, String shortDescTextResId,
                      String longDescTextResId, String mapQueryStrId, String harga) {
        this.imageResourceId = imageResourceId;
        this.titleTextResId = titleTextResId;
        this.shortDescTextResId = shortDescTextResId;
        this.longDescTextResId = longDescTextResId;
        this.mapQueryStrId = mapQueryStrId;
        this.harga = harga;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageResourceId);
        dest.writeString(this.titleTextResId);
        dest.writeString(this.shortDescTextResId);
        dest.writeString(this.longDescTextResId);
        dest.writeString(this.mapQueryStrId);
        dest.writeString(this.harga);
    }


    protected Attraction(Parcel in) {
        imageResourceId = in.readString();
        titleTextResId = in.readString();
        shortDescTextResId = in.readString();
        longDescTextResId = in.readString();
        mapQueryStrId = in.readString();
        harga = in.readString();
    }

    public static final Creator<Attraction> CREATOR = new Creator<Attraction>() {
        @Override
        public Attraction createFromParcel(Parcel in) {
            return new Attraction(in);
        }

        @Override
        public Attraction[] newArray(int size) {
            return new Attraction[size];
        }
    };

    public String getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(String imageResourceId){
        this.imageResourceId = imageResourceId;
    }

    public String getTitle() {
        return titleTextResId;
    }

    public void setTitle(String titleTextResId){
        this.titleTextResId = titleTextResId;
    }

    public String getShortDesc() {
        return shortDescTextResId;
    }

    public void setShortDesc(String shortDescTextResId){
        this.shortDescTextResId = shortDescTextResId;
    }

    public String getLongDesc() {
        return longDescTextResId;
    }

    public void setLongDesc(String longDescTextResId){
        this.longDescTextResId = longDescTextResId;
    }

    public String getMapQueryStrId() {
        return mapQueryStrId;
    }

    public void setMapQueryStrId(String mapQueryStrId){
        this.mapQueryStrId = mapQueryStrId;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga){
        this.harga = harga;
    }
}
