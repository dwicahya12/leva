package com.mobile.tubes.multiuser.model;

import java.util.List;

public class AttractionCollection {

	private String headerTitle;
	private List<Attraction> listOfAttractions;

	public AttractionCollection(String headerTextResId, List<Attraction> listOfAttractions) {
		this.headerTitle = headerTextResId;
		this.listOfAttractions = listOfAttractions;
	}

	public String getHeaderTitle() {
		return headerTitle;
	}

	public List<Attraction> getAttractions() {
		return listOfAttractions;
	}
}
