package com.mobile.tubes.multiuser.model;


import android.annotation.SuppressLint;
import android.app.usage.NetworkStats;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mobile.tubes.multiuser.AdapterCard;
import com.mobile.tubes.multiuser.Card;
import com.mobile.tubes.multiuser.MainActivity;
import com.mobile.tubes.multiuser.R;

import java.util.ArrayList;
import java.util.List;

public class AttractionRepository {

    AdapterCard adapterCard;
    FirebaseAuth mAuth = FirebaseAuth.getInstance() ;

    @SuppressWarnings({"FieldCanBeLocal", "unused"}) private Context context;
    public List<AttractionCollection> collections;
    @SuppressLint("StaticFieldLeak") private static AttractionRepository attractionRepository;

    public static AttractionRepository getInstance(Context packageContext) {
        if (attractionRepository == null) {
            attractionRepository = new AttractionRepository(packageContext);
        }
        return attractionRepository;
    }

    public AttractionCollection getCollection(String sectionTitle) {
        for (int i = 0; i < collections.size(); i++) {
            if (sectionTitle == collections.get(i).getHeaderTitle()) {
                return collections.get(i);
            }
        }

        return null;
    }

    public AttractionRepository(Context context) {
        this.context = context.getApplicationContext();
        collections = new ArrayList<>();

        // Build collection
        AttractionCollection activity = buildActivityCollection();
        collections.add(activity);

        AttractionCollection restaurants = buildRestaurantsCollection();
        collections.add(restaurants);

        AttractionCollection breweries = buildBreweriesCollection();
        collections.add(breweries);

        AttractionCollection nightLife = buildNightLifeCollection();
        collections.add(nightLife);
    }

    public List<AttractionCollection> getCollections() {
        return collections;
    }

    @VisibleForTesting
    public AttractionCollection buildActivityCollection() {


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        List<Attraction> attractions = new ArrayList<>();

        FirebaseFirestore.getInstance().collection("wisata")
                .whereEqualTo("NamaWisata","Dufan").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });


//        FirebaseFirestore.getInstance().collection("wisata").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
        FirebaseFirestore.getInstance().collection("wisata")
                .whereEqualTo("NamaWisata","Gunung Bromo").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });

        FirebaseFirestore.getInstance().collection("wisata")
                .whereEqualTo("NamaWisata","Trans Studio Bandung").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });


//        attractions.add(new Attraction(
//                        R.drawable.jatimpark3,
//                        R.string.horsetooth_mountain_title,
//                        R.string.scenic_open_space,
//                        R.string.horsetooth_mountain_park_detailed_desc,
//                        R.string.mq_jatimpark3
//                )
//        );


        return new AttractionCollection("Place of the week", attractions);
    }

    @VisibleForTesting
    public AttractionCollection buildRestaurantsCollection() {
        ArrayList<Attraction> attractions = new ArrayList<>();

        FirebaseFirestore.getInstance().collection("wisata")
                .whereEqualTo("Jenis","Wisata Modern").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
         @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });

//        attractions.add(new Attraction(
//                        R.drawable.jatimpark3,
//                        R.string.horsetooth_mountain_title,
//                        R.string.scenic_open_space,
//                        R.string.horsetooth_mountain_park_detailed_desc,
//                        R.string.mq_jatimpark3
//                )
//        );


        return new AttractionCollection("Tempat terpopuler" , attractions);
    }

    @VisibleForTesting
    public AttractionCollection buildBreweriesCollection() {
        ArrayList<Attraction> attractions = new ArrayList<>();

        FirebaseFirestore.getInstance().collection(mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });

//        attractions.add(new Attraction(
//                        R.drawable.jatimpark3,
//                        R.string.horsetooth_mountain_title,
//                        R.string.scenic_open_space,
//                        R.string.horsetooth_mountain_park_detailed_desc,
//                        R.string.mq_jatimpark3
//                )
//        );


        return new AttractionCollection("Wisata terdekat dengan anda", attractions);
    }

    @VisibleForTesting
    public AttractionCollection buildNightLifeCollection() {
        ArrayList<Attraction> attractions = new ArrayList<>();

        FirebaseFirestore.getInstance().collection(mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        attractions.add(new Attraction(
                                document.getId(),
                                document.get("NamaWisata").toString(),
                                document.get("ShortDesc").toString(),
                                document.get("LongDesc").toString(),
                                document.get("Maps").toString(),
                                document.get("Harga").toString()));
                    }

                }
            }
        });

//        attractions.add(new Attraction(
//                        R.drawable.jatimpark3,
//                        R.string.horsetooth_mountain_title,
//                        R.string.scenic_open_space,
//                        R.string.horsetooth_mountain_park_detailed_desc,
//                        R.string.mq_jatimpark3
//                )
//        );


        return new AttractionCollection("Destinasi anda selanjutnya", attractions);
    }
}
